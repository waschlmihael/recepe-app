import axios from 'axios';


const axiosInstance = axios.create({
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
    },
});

axiosInstance.interceptors.response.use(
    (response) => {
        return response;
    },
    async function (error) {

        if (typeof error.response === 'undefined') {
            alert(
                'A server/network error occurred. ' +
                'Sorry about this - we will get it fixed shortly.'
            );
            return Promise.reject(error);
        }

        if (
            error.response.data.code === 'token_not_valid' &&
            error.response.status === 401 &&
            error.response.statusText === 'Unauthorized'
        ) {
            console.log(error.response.data)
        }
        else if (error.response.data.code === 402) {
            alert(error.response.data.message)
        }
        console.log(error.response.data)
        return Promise.reject(error);
    }
);

export default axiosInstance;
