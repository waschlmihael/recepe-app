import React, {FC} from 'react';
import styled from "../styled-components";
import {useHistory} from "react-router-dom";
import {addRecipeToSearchedHistory} from "../features/recipe/recipeSlice";
import {useAppDispatch} from "../app/hooks";
import {InRecipeData} from "../types/recipeData.types";

interface Props {
    recipeData: InRecipeData;
}


const RecipeCard: FC<Props> = (props) => {
    const {recipeData} = props

    const history = useHistory();
    const dispatch = useAppDispatch();


    const handleClick = (recipe: any) => {
        history.push(`recipe/${recipe.id}`)
        dispatch(addRecipeToSearchedHistory({
            id: recipe.id,
            title: recipe.title
        }))
    }

    return (
        <Card onClick={() => handleClick(recipeData)}>
            <CardImage src={recipeData.image}/>
            <h4>{recipeData.title}</h4>
            <p>Cooking time: {recipeData.readyInMinutes} minutes</p>
        </Card>
    );
}

export default RecipeCard;

const Card = styled.div`
  width: max(250px, 100%);
  min-height: 300px;
  border: #888888 solid 1px;
  box-shadow: 1px 3px 7px 0 #888888;
  border-radius: 1rem;
  z-index: 2;
  overflow: hidden;
  background-color: var(--clr-primary-200);
  cursor: pointer;
  color: var(--clr-primary-700);

  h4 {
    padding: 0 1.25rem;
    margin: 0.5em 0;
  }

  p {
    font-style: italic;
    font-weight: 400;
    font-size: 0.8rem;
    padding: 0 1.25rem;
  }
`

const CardImage = styled.img`
  width: 100%;
  border-radius: 1rem 1rem 0 0;
`