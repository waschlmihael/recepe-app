import React, {FC} from 'react';
import {Link} from 'react-router-dom';
import styled from "styled-components";

interface Props {
    recipes: Array<{
        id: number,
        title: string
    }>,
    title: string
}

const RecipeList: FC<Props> = (props) => {
    const {recipes, title} = props
    return (
        <RecipeListContainer>
            <Heading>{title}</Heading>
            <table>
                <tbody>
                {recipes.map((recipe, index) => (
                    <tr key={index}>
                        <td>
                            <Link to={`/recipe/${recipe.id}`}>
                                {recipe.title}
                            </Link>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </RecipeListContainer>
    );
}

export default RecipeList;

const RecipeListContainer = styled.div`
  background-color: ${props => props.theme.fontColor};
  border-radius: 1rem;
  color: ${props => props.theme.body};
  padding: 1rem;
  box-shadow: 1px 3px 7px 0 #888888;
  width: min(calc(570px - 2.5rem), 100%);
  margin: 2rem 0;


  table {
    td {
      padding: 0.3rem 0;
    }

    tr {
      cursor: pointer;
      border-bottom: var(--clr-primary-700) 1px solid;
      transition: color 0.4s ease;

      &:hover {
        color: var(--clr-primary-400);
      }
    }
  }
`

const Heading = styled.h3`
  color: var(--clr-primary-400);
`