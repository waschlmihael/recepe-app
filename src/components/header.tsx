import React, {FC} from 'react';
import {useHistory} from 'react-router-dom';
import styled from "styled-components";

import LOGO from "../images/logo_transparent.svg"
import ToggleSwitch from "./toggleSwitch";

interface Props {
    theme: string;
    onToggle: () => void;
}

const Header: FC<Props> = (props) => {
    const {theme, onToggle} = props;

    const history = useHistory();

    const redirectToHome = () => {
        history.push(`/`);
    }

    return (
        <HeaderContainer>
            <HeaderLogo src={LOGO} alt="Logo" onClick={redirectToHome}/>
            <ToggleSwitch isToggled={theme === "dark"} onToggle={onToggle}/>
        </HeaderContainer>
    );
}

export default Header;

const HeaderContainer = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  z-index: 10;
  padding: 1.25rem 1.55rem;
  height: var(--header-height);
`

const HeaderLogo = styled.img`
  height: inherit;
  width: inherit;
  cursor: pointer;
`


