import React, {FC} from 'react';
import styled from "../styled-components";

import Dropdown from "./dropdown";

interface Props {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
    searchData: string;
    recipes: Array<{
        id: number;
        title: string;
    }>
}

const SearchBar: FC<Props> = (props) => {
    const {onChange, searchData, onSubmit, recipes} = props
    return (
        <Container>
            <Form onSubmit={onSubmit} role="search">
                <input
                    id="search" type="search"
                    placeholder="Search..."
                    autoFocus required
                    onChange={onChange}
                    value={searchData}
                    autoComplete="off"
                />
                <button type="submit">Find</button>
            </Form>
            <Dropdown style={recipes?.length === 0 ? {height: 0, padding: 0} : {height: "200px"}} recipes={recipes}/>
        </Container>
    );
}

export default SearchBar;

const Container = styled.div`
  position: relative;
  width: max(50%, 300px);
`

const Form = styled.form`
  position: relative;
  width: 100%;
  border-radius: 0.7rem 0.7rem 0 0;
  background: var(--clr-primary-400);

  input, button {
    height: 3rem;
    border: 0;
    font-size: 1.2rem;
    color: ${props => props.theme.body}
  }

  input[type="search"] {
    outline: 0;
    width: 100%;
    background: ${props => props.theme.fontColor};
    padding: 0 1.6rem;
    border-radius: 0.7rem 0.7rem 0 0;
    appearance: none;
    transition: all 0.3s ease-out;
    transition-property: width, border-radius;
    z-index: 1;
    position: relative;
  }

  button {
    display: none;
    position: absolute;
    top: 0;
    right: 0;
    width: 6rem;
    font-weight: bold;
    border-radius: 0.7rem 0.7rem 0 0;
    cursor: pointer;
    background-color: var(--clr-primary-400);
    padding: 1rem;
  }

  input:not(:placeholder-shown) {
    border-radius: 0.7rem 0 0 0;
    width: calc(100% - 6rem);
  }

  input:not(:placeholder-shown) + button {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  label {
    position: absolute;
    cursor: pointer;
    clip: rect(1px, 1px, 1px, 1px);
    padding: 0;
    border: 0;
    height: 1px;
    width: 1px;
    overflow: hidden;
    color: ${props => props.theme.fontColor}
  }
`