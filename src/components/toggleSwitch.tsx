import React, {FC} from 'react';
import styled from "styled-components";

interface Props {
    isToggled: boolean;
    onToggle: () => void;
    style?: object;
}

const ToggleSwitch: FC<Props> = (props) => {
    const {isToggled, onToggle, style} = props

    return (
        <Switch style={style}>
            <input type="checkbox" checked={isToggled} onChange={onToggle}/>
            <span className="slider"/>
        </Switch>
    );
}

export default ToggleSwitch;

const Switch = styled.label`
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;

  input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  input:checked + .slider:before {
    transform: translateX(26px);
  }

  input:checked + .slider {
    background-color: var(--clr-primary-400);
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background-color: gray;
    transition: background-color 0.4s ease-out;
    border-radius: 24px;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    transition: transform 0.4s ease-in;
    border-radius: 50%;
  }
`