import React, {FC} from 'react';
import styled from "styled-components";
import {useHistory} from "react-router-dom";
import {addRecipeToSearchedHistory, InRecipe} from "../features/recipe/recipeSlice";
import {useAppDispatch} from "../app/hooks";

interface Props {
    recipes: Array<InRecipe> | undefined;
    style?: object;
}

const Dropdown: FC<Props> = (props) => {
    const {recipes, style} = props
    const dispatch = useAppDispatch();
    const history = useHistory();

    const selectRecipe = (recipe: InRecipe) => {
        history.push(`recipe/${recipe.id}`)
        dispatch(addRecipeToSearchedHistory({
            id: recipe.id,
            title: recipe.title
        }))
    }

    if (recipes !== undefined) {
        return (
            <Container style={style}>
                <table>
                    <tbody>
                    {recipes?.map((recipe, index) => (
                        <tr key={index} onClick={() => selectRecipe({id: recipe.id, title: recipe.title})}>
                            <td>
                                {recipe.title}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </Container>
        );
    } else return null
}

export default Dropdown;

const Container = styled.div`
  width: 100%;
  background-color: ${props => props.theme.fontColor};
  padding: 0 1.25rem 1.25rem 1.25rem;
  border-radius: 0 0 0.7rem 0.7rem;
  transition: height 0.5s ease-in;

  table {
    width: 100%;
  }

  td {
    padding: 0.3rem 0;
  }

  tr {
    cursor: pointer;
    border-bottom: var(--clr-primary-700) 1px solid;
    transition: color 0.4s ease, background-color 0.4s ease;
    color: ${props => props.theme.body};

    &:hover {
      color: var(--clr-primary-400);
      background-color: ${props => props.theme.body};;
    }
  }


`