import {createGlobalStyle} from "./styled-components";

export interface ThemeInterface {
    body: string;
    fontColor: string;
}

export const lightTheme = {
    body: "var(--clr-primary-200)",
    fontColor: "var(--clr-primary-700)"
}

export const darkTheme = {
    body: "var(--clr-primary-700)",
    fontColor: "var( --clr-primary-200)"
}

export const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${props => props.theme.body};
    color: ${props => props.theme.fontColor};
  }
`