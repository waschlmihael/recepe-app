import React, {useState} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import styled, {ThemeProvider} from "styled-components";
import {lightTheme, darkTheme, GlobalStyle} from "./themes";

import HomePage from "./pages/homePage";
import RecipePage from "./pages/recipePage";
import Header from "./components/header";
import NoMatch from "./pages/noMatch";

import './App.css';
import ServingDishLogo from './components/svgs/servingDishLogo';
import ForkLogo from "./components/svgs/forkLogo";

function App() {
    const [theme, setTheme] = useState<"light" | "dark">("dark");

    const toggleTheme = () => {
        if (theme === "light") {
            setTheme("dark")
        } else {
            setTheme("light")
        }
    }

    return (
        <ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
            <GlobalStyle/>
            <Router>
                <Header theme={theme} onToggle={toggleTheme}/>
                <MainContainer>
                    <Switch>
                        <Route path="/" exact component={HomePage}/>
                        <Route path="/recipe/:recipe_id" exact component={RecipePage}/>
                        <Route path="*" component={NoMatch}/>
                    </Switch>
                    <ForkLogo theme={theme}/>
                    <ServingDishLogo theme={theme}/>
                </MainContainer>
            </Router>
        </ThemeProvider>
    );
}

export default App;

const MainContainer = styled.main`
  position: relative;
  width: 80%;
  height: var(--main-height);
  padding: calc(var(--header-height) + 1rem) 0 1.25rem 0;
  margin: 0 auto;
`
