import React, {FC} from 'react';
import styled from "styled-components";

interface Props {

}

const NoMatch: FC<Props> = (props) => {
    return (
        <MainSection>No page found</MainSection>
    );
}

export default NoMatch;

const MainSection = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`
