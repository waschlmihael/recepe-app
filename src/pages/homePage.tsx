import React, {FC, useEffect, useState} from 'react';
import styled from "styled-components";
import RecipeList from "../components/recipeList";
import SearchBar from "../components/searchBar";
import axiosInstance from "../axiosInstance";
import {BASE_URL} from "../constants";
import {useHistory} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {
    addRecipeToSearchedHistory,
    recipeSearchHistory,
    fetchRandomRecipes,
    randomRecipesData
} from "../features/recipe/recipeSlice";
import RecipeCard from "../components/recipeCard";

interface Props {

}

const HomePage: FC<Props> = (props) => {
    const history = useHistory();
    const dispatch = useAppDispatch();

    //component state
    const [searchData, setSearchData] = useState<string>("")
    const [recipes, setRecipes] = useState<Array<{
        id: number;
        title: string;
    }>>([])

    //Redux connections
    const recipeSearchedHistory = useAppSelector(recipeSearchHistory)
    const randomRecipes = useAppSelector(randomRecipesData)

    useEffect(() => {
        dispatch(fetchRandomRecipes())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleSearchBarChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setSearchData(value)
        getRecipes(value)
            .then(autoComplitedRecipes => {
                setRecipes(autoComplitedRecipes)
            })
    }

    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        history.push(`/recipe/${recipes[0].id}`)
        dispatch(addRecipeToSearchedHistory({
            id: recipes[0].id,
            title: recipes[0].title
        }))
    }

    const getRecipes = async (queryData: string) => {
        const response = await axiosInstance.get(`${BASE_URL}/recipes/autocomplete?number=5&query=${queryData}&apiKey=${process.env.REACT_APP_API_KEY}`)
        return response.data
    }

    return (
        <MainSection>
            <LandingPage>
                <HeadingContainer>
                    <MainHeading>Find your perfect <span>recipe</span>!</MainHeading>
                    <SearchBar
                        onSubmit={onSubmit}
                        onChange={handleSearchBarChange}
                        searchData={searchData}
                        recipes={recipes}
                    />
                </HeadingContainer>
                {
                    recipeSearchedHistory.length !== 0
                        ? <RecipeList
                            recipes={recipeSearchedHistory.slice(Math.max(recipeSearchedHistory.length - 5, 0)).reverse()}
                            title={"Search history"}/>
                        : null

                }
            </LandingPage>
            <div>
                <h3 style={{textAlign: "center", paddingBottom: "2rem"}}>Recomendations</h3>
                <CardContainer>
                    {
                        randomRecipes?.map((recipe, index) => (
                            <RecipeCard key={index} recipeData={recipe}/>
                        ))
                    }
                </CardContainer>
            </div>
        </MainSection>
    );
}

export default HomePage;

const MainSection = styled.div`
  flex-direction: row;
  margin: auto 0;
  padding-bottom: 5rem;
  min-height: calc(100vh - var(--header-height));
`

const MainHeading = styled.h1`
  max-width: 10ch;
  letter-spacing: 0.1em;
  text-align: center;

  span {
    color: var(--clr-primary-400);
  }

  @media (max-width: 400px) {
    font-size: 3.052rem !important;
  }
`

const LandingPage = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: calc(100vh - var(--header-height));
`


export const CardContainer = styled.div`
  width: 100%;
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  justify-items: center;
  grid-gap: 2rem;

  @media (min-width: 1500px) {
    grid-template-columns: repeat(4, minmax(300px, 1fr));
  }
`

const HeadingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

