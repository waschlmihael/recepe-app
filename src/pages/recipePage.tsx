import React, {FC, useEffect, useRef, useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import axiosInstance from "../axiosInstance";
import {BASE_URL} from "../constants";
import {InRecipeData} from "../types/recipeData.types";
import styled from "styled-components";
import {IoIosArrowDropdownCircle} from 'react-icons/io';
import ToggleSwitch from '../components/toggleSwitch';
import {addRecipeToSearchedHistory} from "../features/recipe/recipeSlice";
import {useAppDispatch} from "../app/hooks";
import Spinner from '../components/spinner';

interface Props {

}

interface RouteParams {
    recipe_id: string
}

interface SimilarRecepies {
    id: number;
    imageType: string;
    readyInMinutes: number;
    servings: string;
    sourceUrl: string;
    title: string;
}


const RecipePage: FC<Props> = (props) => {
    //Component state
    const [recipeData, setRecipeData] = useState<InRecipeData | undefined>(undefined)
    const [similarRecipes, setSimilarRecipes] = useState<Array<SimilarRecepies>>([])
    const [unit, setUnit] = useState<"metric" | "us">("metric")
    const [isDescriptionDisplayed, setDescriptionDisplayed] = useState<boolean>(false)
    const [isRecepieLoading, setIsRecipeLoading] = useState<boolean>(false)

    let {recipe_id} = useParams<RouteParams>();
    const mainEl = useRef(null);
    const history = useHistory();
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (recipe_id && !isNaN(parseInt(recipe_id))) {
            if (mainEl.current) {
                window.scrollTo({
                    behavior: "smooth",
                    // @ts-ignore
                    top: mainEl.current.offsetTop
                });
            }
            getRecipeData(recipe_id).then(recipe => {
                setRecipeData(recipe)
                setIsRecipeLoading(false)
            })
            getSimilarRecipes(recipe_id).then(recipes => {
                setSimilarRecipes(recipes)
            })

            return () => {
                setSimilarRecipes([])
                setRecipeData(undefined)
            }
        } else {
            history.push("/")
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [recipe_id])

    const getRecipeData = async (id: string) => {
        try {
            setIsRecipeLoading(true)
            const response = await axiosInstance.get(`${BASE_URL}/recipes/${id}/information?apiKey=${process.env.REACT_APP_API_KEY}`)
            return response.data
        } catch (err) {
            setIsRecipeLoading(false)
            if (err.response.data.status === 404) {
                alert(err.response.data.message)
            }
            console.log(err.response.data)
        }
    }

    const getSimilarRecipes = async (id: string) => {
        try {
            const response = await axiosInstance.get(`${BASE_URL}/recipes/${id}/similar?number=4&apiKey=${process.env.REACT_APP_API_KEY}`)
            return response.data
        } catch (err) {
            if (err.response.data.status === 404) {
                alert(err.response.data.message)
            }
            console.log(err.response.data)
        }

    }

    const redirectToSimilarRecipe = (id: number, title: string) => {
        history.push(`/recipe/${id}`)
        dispatch(addRecipeToSearchedHistory({
            id: id,
            title: title
        }))
    }

    const onUnitToggle = () => {
        if (unit === "metric") {
            setUnit("us")
        } else {
            setUnit("metric")
        }
    }

    const toggleDescription = () => {
        setDescriptionDisplayed(prevState => !prevState)
    }
    if (isRecepieLoading) {
        return <Spinner/>
    }
    return (
        recipeData ?
            <RecipeContainer ref={mainEl}>
                <RecipeHeadingContainer>
                    <h2>{recipeData.title}</h2>
                    <p>Total cooking time: {recipeData.readyInMinutes} minutes</p>
                </RecipeHeadingContainer>
                <RecipeImage src={recipeData.image}/>
                <Description>
                    <h3>Description</h3>
                    <p className={isDescriptionDisplayed ? "full-text" : ""}
                       dangerouslySetInnerHTML={{__html: recipeData.summary}}/>
                    <DropdownArrow size="30px" onClick={toggleDescription}
                                   className={isDescriptionDisplayed ? "rotate" : ""}/>
                </Description>
                <Ingredians>
                    <h3>Ingredians</h3>
                    <SplitContainer>
                        <p style={{fontSize: "0.8rem"}}>Units</p>
                        <ToggleSwitch
                            isToggled={unit === "metric"}
                            onToggle={onUnitToggle}
                            style={{transform: "scale(0.6)"}}
                        />
                    </SplitContainer>
                    <ul>
                        {recipeData.extendedIngredients.map((ingredient, index) => {
                            const measure = Math.round((ingredient.measures[unit].amount) * 100) / 100
                            return <li key={index}>
                                {measure} {ingredient.measures[unit].unitShort} - {ingredient.name}
                            </li>
                        })}
                    </ul>
                </Ingredians>
                <Instructions>
                    <h3>How to make it</h3>
                    {recipeData.analyzedInstructions.map((instruction, index) => (
                        instruction.steps.map((step, step_index) => {
                            return <li key={`${index}_${step_index}`}>{step.step}</li>
                        })
                    ))}
                </Instructions>
                {similarRecipes
                    ? <SimilarRecipes>
                        <h3>Similar recipes</h3>
                        <div className="similar-recipes">
                            {
                                similarRecipes.map(recipe => (
                                    <div id={recipe.id.toString()} className="recipe" key={recipe.id}
                                         onClick={() => redirectToSimilarRecipe(recipe.id, recipe.title)}>
                                        <span>{recipe.title}</span>
                                    </div>
                                ))
                            }
                        </div>
                    </SimilarRecipes>
                    : null}
            </RecipeContainer>
            : null
    );
}

export default RecipePage;

const RecipeContainer = styled.div`
  padding: 1.25rem 0 8rem 0;

  h2 {
    margin-bottom: 1rem;
  }

  h3 {
    color: var(--clr-primary-400);
  }

  @media (min-width: 768px) {
    display: grid;
    width: 80%;
    margin: 0 auto;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: auto;
    gap: 1rem 3rem;
    grid-template-areas: 
    "heading heading"
    "description image"
    "instructions ingredians"
    "similar-recepies similar-recepies"
  }
`
const RecipeHeadingContainer = styled.div`
  grid-area: heading;

  p {
    font-style: italic;
    font-weight: 600;
    font-size: 0.8rem;
  }
`

const RecipeImage = styled.img`
  grid-area: image;
  width: 100%;

  @media (min-width: 768px) {
    margin: 1.5rem 0;
    width: 100%;
  }
`

const Description = styled.div`
  grid-area: description;
  margin: 2rem 0;

  p {
    height: 150px;
    overflow: hidden;
    transition: height 0.3s ease-in;

    &.full-text {
      height: auto;
    }
  }

  span {
    cursor: pointer;
    display: inline-block;
    width: 100%;
    background-color: gray;
  }

  @media (min-width: 768px) {
    margin: 0;
    p {
      height: auto;
    }
  }
`

const Ingredians = styled.div`
  grid-area: ingredians;
  margin: 2rem 0;

  @media (min-width: 768px) {
    margin: 0;
  }
`

const Instructions = styled.ol`
  grid-area: instructions;
  list-style: none;
  counter-reset: my-awesome-counter;
  margin: 2rem 0;
  padding: 0;

  li {
    counter-increment: my-awesome-counter;
    display: flex;
    font-size: 0.8rem;
    margin-bottom: 0.5rem;
    color: ${props => props.theme.fontColor};
  }

  li::before {
    content: "0" counter(my-awesome-counter);
    font-weight: bold;
    font-size: 3rem;
    margin-right: 0.5rem;
    font-family: 'Abril Fatface', serif;
    line-height: 1;
  }

  @media (min-width: 768px) {
    margin: 0;
  }
`

const SimilarRecipes = styled.div`
  grid-area: similar-recepies;
  margin: 2rem 0;

  .similar-recipes {
    display: flex;
    align-items: center;
    justify-content: space-evenly;

    .recipe {
      padding: 0 1rem;
      border-right: 1px solid grey;
      cursor: pointer;

      span {
        transition: transform 0.5s ease-out;

        &:hover {
          transform: scale(1.2);
        }
      }
    }

    .recipe:last-of-type {
      border-right: none;
    }

    @media (max-width: 768px) {
      flex-direction: column;

      .recipe {
        margin: 1rem 0;
        border-bottom: 1px solid grey;
        border-right: none;
        width: 100%;
      }
    }
  }

  @media (min-width: 768px) {
    margin: 0;
  }
`

//Support containers
const SplitContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;

  p {
    margin: 0;
    padding: 0;
  }
`

const DropdownArrow = styled(IoIosArrowDropdownCircle)`
  display: flex;
  width: 100%;
  align-items: center;
  color: ${props => props.theme.backgroundColor};
  cursor: pointer;
  transition: transform 0.5s ease-in;

  &.rotate {
    transform: rotate(180deg);
  }

  @media (min-width: 768px) {
    display: none;
  }
`