import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from "../../app/store";
import axiosInstance from "../../axiosInstance";
import {BASE_URL} from "../../constants";
import {InRecipeData} from "../../types/recipeData.types";

export interface RecipeState {
    searchedRecipes: Array<InRecipe>
    randomRecipes: Array<InRecipeData>
}

export interface InRecipe {
    id: number,
    title: string
}


const initialState: RecipeState = {
    searchedRecipes: [],
    randomRecipes: []
};

export const fetchRandomRecipes = createAsyncThunk(
    'recipe/fetchRandomRecipes',
    async () => {
        const response = await axiosInstance.get(`${BASE_URL}/recipes/random?number=8&apiKey=${process.env.REACT_APP_API_KEY}`)
        return response.data as InRecipeData;
    }
);


export const recipeSlice = createSlice({
    name: 'recipe',
    initialState,
    reducers: {
        addRecipeToSearchedHistory: (state, action: PayloadAction<InRecipe>) => {
            state.searchedRecipes = [...state.searchedRecipes, action.payload]
        },
        clearSearchedHistory: (state) => {
            state.searchedRecipes = []
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchRandomRecipes.pending, (state) => {
                // state.randomRecipes = 'loading';
            })
            .addCase(fetchRandomRecipes.fulfilled, (state, action) => {
                // @ts-ignore
                state.randomRecipes = action.payload.recipes;
            });
    },

});

export const {addRecipeToSearchedHistory, clearSearchedHistory} = recipeSlice.actions;

export const recipeSearchHistory = (state: RootState) => state.recipe.searchedRecipes;
export const randomRecipesData = (state: RootState) => state.recipe.randomRecipes;

export default recipeSlice.reducer;
