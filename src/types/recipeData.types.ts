export interface InRecipeData {
    analyzedInstructions: Array<InInstructions>;
    title: string;
    image: string;
    servings: string;
    readyInMinutes: number;
    healthScore: number;
    spoonacularScore: number;
    cuisines: Array<string>;
    diets: Array<string>;
    dishTypes: Array<string>;
    extendedIngredients: Array<InIngridiant>;
    summary: string;
    winePairing: {
        pairedWines: Array<string>;
        pairingText: string;
        productMatches: Array<InProductMatch>
    }

}

interface InInstructions {
    name: string;
    steps: Array<InStep>
}

interface InStep {
    number: number;
    step: string;

}

interface InProductMatch {
    id: number;
    title: string;
    description: string;
    price: string;
    imageUrl: string;
    averageRating: number;
    ratingCount: number;
    score: number;
    link: string;
}

interface InIngridiant {
    name: string;
    original: string;
    originalName: string;
    unit: string;
    measures: {
        metric: InMeasure;
        us: InMeasure;
    }
}

interface InMeasure {
    amount: number;
    unitLong: string;
    unitShort: string;
}